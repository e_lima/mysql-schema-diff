mysql-schema-diff
--------------------------------------------------------------------------------
Transformation script generator for MySQL


             Parameters
--------------------------------------------------------------------------------
          --source-user [optional]
          
                        User for connecting to source database
                        
                        default: "root"
                        sample: --source-user "root"


          --source-host [optional]
          
                        Host of source database
                        
                        default: "localhost"
                        sample: --source-host "localhost:3306"


      --source-database [required] 
      
                        Name of the source database (schema)
                        
                        sample: --source-database="source-db"


          --target-user [optional] 
          
                        User for connecting to source database
          
                        default: "root"
                        sample: --target-user "root"


          --target-host [optional] 
          
                        Host of target database
          
                        default: "localhost"
                        sample: --target-host "localhost:3306"
          
                        
      --target-database [required] 
                        
                        Name of the source database (schema)
                        
                        sample: --source-database "source-db"
                        

 --create-if-not-exists [optional] 
                        
                        Adds "IF NOT EXISTS" for CREATE statements
                        
                        default: true
                        sample: --create-if-not-exists false


            --mysqldump [optional] 
                        
                        Sets the path for the mysqldump command
                        
                        detault: "mysqldump" (from PATH)
                        sample: --mysqldump "/var/etc/mysql/mysqldump"


       --mysqldbcompare [optional]
                        
                        Sets the path for the mysqldbcompare command
                        
                        default: "mysqldbcompare" (from PATH)
                        
                        sample: 
                        --mysqldbcompare "/var/etc/mysql/utils/mysqldbcompare"


               --silent [optional]
                        
                        Turn on/off writing output to the stdout 
                        
                        detault: false
                        sample: --silent true
                        
                        
              --verbose [optional]
                        
                        Turn on/off writing verbose logs to the stdout 
                        
                        detault: false
                        sample: --verbose true
                        
                        
                 --file [optional]
                        
                        Sets the file for writing the generated SQL scripts
                        
                        sample: --file "dump.sql"
                        
                        
               --params [optional]
                        
                        Sets the JSON file for reading the params for execution
                        
                        sample: --params "params.json"


--------------------------------------------------------------------------------

