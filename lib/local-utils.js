/**
 * Provides the utility classes and functions for internal use
 *
 * @module utility
*/


/**
 * Internal utility functions
 *
 * @class LocalUtils
 */ 
var util = require('util');

var instance = (function LocalUtils() {
  
   var self = this;
  /**
   * Instance self reference
   * @property self
   * @type {LocalUtils}
  */
  this.self = this;

  /**
   * @property createObjectRegEx
   * @type String
   * @default /^CREATE\s(\w+)\s(.*)(\()?$/
   */
  this.createObjectRegEx = /^CREATE\s(\w+)\s(.*)(\()?$/;

  /**
   * Changes the default CREATE TABLE statement to CREATE TABLE IF NOT EXISTS like statement
   *
   * @method ifNotExistsTransform
   * @author Edimar Lima 
   * @param {String} someString The script statement line
   * @return {String} The transformed command
  */
  util.ifNotExistsTransform = function(someString) {
    return someString.replace(self.createObjectRegEx, 'CREATE $1 IF NOT EXISTS $2$3');
  }

  /**
   * Checks if @obj param doesn't contains any @requiredParamsList keys / properties
   *
   * @method requiredValidation
   * @author Edimar Lima 
   * @param {Object} obj The object to check
   * @param {Array} requiredParamsList The keys to check against @obj
   * @throws {Error} Param "%s" not supplied
  */
  util.requiredValidation = function(obj, requiredParamsList) {

    for(var s in requiredParamsList) {
      if (!obj[requiredParamsList[s]]) {
        var e = new Error(util.format('Param "%s" not supplied', requiredParamsList[s]));
        throw e.message;
      }
    }

  }
  
  return this;

})();

module.exports = instance;