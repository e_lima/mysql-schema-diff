/**
 * Provides the utility class for dealing with mysql utilities and commands
 *
 * @module core
*/
var child_process = require('child_process');
var path = require('path');
var util = require('util');
var localUtils = require('./local-utils');
var byline = require('byline');

/**
 * Provides the utility functions for mysql compare and mysql dump
 *
 * @class MySqlSchemaDiff
*/
var MySqlSchemaDiff = {

  /**
   * Wrapper for mysqldump command
   *
   * @method MySqlDump
   * @async
   * @author Edimar Lima 
   * @param {Object} instanceInfo Parameters for command execution
   * @param {String} instanceInfo.source-user User to use when connecting to source host database
   * @param {String} instanceInfo.source-host Source database host name
   * @param {String} instanceInfo.source-password Password to use when connecting to source host database
   * @param {String} instanceInfo.source-database Schema/Database on source host
   * @param {function} callback Callback for returning data or error `cb(err, contents)`
  */
  MySqlDump: function(instanceInfo, callback) {

    if (!instanceInfo) 
      throw new Error('MySqlDump: params not specified');

    if (typeof callback !== 'function')
      throw new Error('MySqlDump: callback not specified');

    util.requiredValidation(
      instanceInfo, 
      [
        'source-user', 
        'source-host',
        'source-database'
      ]
    )

    var command = instanceInfo.mysqldump || 'mysqldump';

    var commandArgs = [];
    commandArgs.push('-u');
    commandArgs.push(instanceInfo['source-user']);
    commandArgs.push('-h');
    commandArgs.push(instanceInfo['source-host']);
    
    if (instanceInfo['source-password']) commandArgs.push('-p' + instanceInfo['source-password']);
    
    commandArgs.push(instanceInfo['source-database']);

    commandArgs.push('-d');
    commandArgs.push('--add-drop-table=false');

    var dumpContent = "";

    var c = child_process.spawn(command, commandArgs);
    var callError = null;
    var errContent = null;

    var lineStream = byline(c.stdout);
    lineStream.on('data', function(lineContent) {

      var lineString = lineContent.toString();
      if (instanceInfo['create-if-not-exists'] && lineString.indexOf('CREATE') > -1)  {
        lineContent = util.ifNotExistsTransform(lineString);
      }

      dumpContent += "\r\n" + lineContent;
    })
    
    c.stderr.on('data', function(chunk) {
      errContent = errContent || '';
      errContent += chunk.toString();
    })

    c.on('error', function(err) {
      callError = err;
    })

    c.on('close', function(code) {
      callError = callError || errContent;
      callback(callError, dumpContent);
    });

  },

  /**
   * Wrapper for mysqldbcompare command
   *
   * @method MySqlDiff
   * @async
   * @author Edimar Lima 
   * @param {Object} instanceInfo Parameters for command execution
   * @param {String} instanceInfo.source-user User to use when connecting to source host database
   * @param {String} instanceInfo.source-host Source database host name
   * @param {String} instanceInfo.source-password Password to use when connecting to source host database
   * @param {String} instanceInfo.source-database Schema/Database on source host
   * @param {String} instanceInfo.target-user User to use when connecting to target host database
   * @param {String} instanceInfo.target-host Target database host name
   * @param {String} instanceInfo.target-password Password to use when connecting to target host database
   * @param {String} instanceInfo.target-database Schema/Database on target host
   * @param {function} callback Callback for returning data or error `cb(err, contents)`
  */
  MySqlDiff: function(instanceInfo, callback) {

    if (!instanceInfo) 
      throw new Error('MySqlDiff: params not specified');

    if (typeof callback !== 'function')
      throw new Error('MySqlDiff: callback not specified');

    util.requiredValidation(
      instanceInfo, 
      [
        'source-user', 
        'source-host', 
        'source-database',
        'target-user', 
        'target-host', 
        'target-database'
      ]
    )

    var command = instanceInfo.mysqldbcompare || 'mysqldbcompare';

    var commandArgs = [];
    commandArgs.push(
      util.format(
        '--server1=%s%s@%s', 
        instanceInfo['source-user'], 
        instanceInfo['source-password'] ? ':' + instanceInfo['source-password'] : '',
        instanceInfo['source-host']
      )
    );
    
    commandArgs.push(
      util.format(
        '--server2=%s%s@%s', 
        instanceInfo['target-user'], 
        instanceInfo['target-password'] ? ':' + instanceInfo['target-password'] : '', 
        instanceInfo['target-host']
      )
    );
    
    commandArgs.push(util.format('`%s`:`%s`', instanceInfo['source-database'], instanceInfo['target-database']));
    
    commandArgs.push('--changes-for=server2');
    commandArgs.push('--difftype=sql');
    commandArgs.push('--run-all-tests');
    commandArgs.push('--skip-table-options');
    commandArgs.push('--skip-data-check');
    commandArgs.push('--skip-row-count');
    commandArgs.push('-c');
    
    //console.log(commandArgs.join(' '));

    var diffContent = "";

    var c = child_process.spawn(command, commandArgs);

    var lineStream = byline(c.stdout);
    lineStream.on('data', function(lineContent) {

      var fl = lineContent.toString()[0];

      if (fl === '+' ||
          fl === '-' ) {
        lineContent = '#' + lineContent;
      }

      diffContent += "\r\n" + lineContent;
    })

    var callError = null;
    var errContent = null;

    c.on('error', function(err) {
      callError = err;
    })
    
    c.stderr.on('data', function(chunk) {
      errContent = errContent || '';
      errContent += chunk.toString();
    })

    c.on('close', function(code) {
      callError = callError || errContent;
      callback(callError, diffContent);
    });
  }
}

module.exports = MySqlSchemaDiff;