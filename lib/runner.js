/**
 * Runner module for command line execution (CLI)
 * @module runner
*/
var mySqlSchemaDiff = require('./');
var util = require('util');
var fs = require("fs");
var _ = require('lodash');

/**
 * Runner class
 * @class Runner
 * @constructor
 * @param {Object} options Runner execution parameters
*/
function Runner(options) {
  
  this.defaults = {
    silent: false,
    verbose: false,
    'create-if-not-exists': true,
    'source-host': 'localhost',
    'target-host': 'localhost',
    'source-user': 'root',
    'target-user': 'root'
  }
  
  _.defaults(options, this.defaults);
  
  /**
   * Instance options
   * @property options
   * @type Object
  */
  this.options = options;
  
  if (!options) throw 'Options not supplied';
  
  var self = this;
  /**
   * Instance self reference
   * @property self
   * @type Runner
  */
  this.self = this;
  
  /**
   * Verbose level logging function
   * @method verbose
   * @param {String} string Text to log
  */
  this.verbose = function(string) {
    if (options.verbose) {
      console.log(string);
    }
  }

  /**
   * Info level logging function
   * @method info
   * @param {String} string Text to log
  */
  this.info = function(string) {
    if (!options.silent) {
      console.log(string);
    }
  }

  self.verbose('Arguments');
  self.verbose(options);
  
  /**
   * Loads parameters from json file
   * @method loadParams
   * @param {String} file JSON params file path
  */
  this.loadParams = function(file){
    var paramsJSON = fs.readFileSync(file, { encoding: 'utf-8' });
    options = JSON.parse(paramsJSON);
    self.verbose('Loaded arguments');
    self.verbose(options);
    self.options = options;
  }
  
  if (options.params) {
    this.loadParams(options.params);
  }

  var instanceInfo = options; 
  
  /**
   * Executes the schema diff with the current options
   * @method run
   * @param {Function} callback Callback function for returning err / contents `cb(err, contents)`
   * @async
  */
  this.run = function(callback) {
    
    if (instanceInfo.help) {
      var rs = fs.createReadStream("help.txt", { encoding: "utf-8" });
      return rs.pipe(process.stdout);
    }

    self.info('Starting dump');
    
    mySqlSchemaDiff.MySqlDump(
      instanceInfo,
      function(err, dumpContent) {

        if (err) { 
          if (callback) {
            return callback(err);
          }
          else {
            console.log(err);
            return;
          }
        }
        
        self.info('Dump finished');
        self.verbose('Dump content:\r\n' + dumpContent);

        self.info('Starting diff');
        mySqlSchemaDiff.MySqlDiff(
          instanceInfo,
          function(err, diffContent) {

            if (err) { 
              if (callback) {
                return callback(err);
              }
              else {
                console.log(err);
                return;
              }
            }
            
            self.info('Diff finished');
            self.verbose(diffContent);

            if (instanceInfo.file) {
              self.info('Writing to file: ' + instanceInfo.file);

              s = fs.createWriteStream(instanceInfo.file);
              s.write(dumpContent, 'utf-8');
              s.write(diffContent, 'utf-8');
              s.end();
            }
            
            if (callback) {
              callback(null, [ dumpContent, diffContent ]);
            }

          }
        )
      }
    );
  }
  
}

/**
 * Creates a new Runner with the specified options
 * @method createRunner
 * @return {Runner} A new Runner instance
 * @param {Function} callback Callback function for returning err / contents `cb(err, contents)`
*/
Runner.createRunner = function(options) {
  return new Runner(options);
}

module.exports = Runner;