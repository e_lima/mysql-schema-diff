var minimist = require('minimist');
var args = minimist(process.argv.slice(2));

if (args.run || args._.indexOf('run') > -1 || args.help) {
  var Runner = require('./lib/runner');
  var i = new Runner(args);
  i.run();
}

var mySqlSchemaDiff = require('./lib');
module.exports = mySqlSchemaDiff;