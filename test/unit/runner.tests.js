var child_process = require('child_process');

describe('Runner tests', function() {
  
  it('Invalid uses', function(done) {
    
    should.Throw(function() {
      var i = Runner.createRunner();
    })
    
    should.not.Throw(function() {
      var i = Runner.createRunner({});
    })
    
    //no source-user
    should.Throw(function() {
      var i = new Runner({});
      i.run();
    })
    
    //no source-user
    should.Throw(function() {
      var i = new Runner({ 'source-user': '123' });
      i.run();
    })
    
    //no source-user
    should.not.Throw(function() {
      var i = new Runner(
        { 
          'source-user': '123',
          'source-host': '123',
          'source-password': '123',
          'source-database': '123',
          'target-user': '123',
          'target-host': '123',
          'target-password': '123',
          'target-database': '123',
          'silent': true
        }
      );
      i.run();
    })
    
    //no source-user
    should.not.Throw(function() {
      var i = new Runner(
        { 
          'source-user': '123',
          'source-host': '123',
          'source-password': '123',
          'source-database': '123',
          'target-user': '123',
          'target-host': '123',
          'target-password': '123',
          'target-database': '123',
          'verbose': true
        }
      );
      i.run(function(err, contents) {
        
        should.exist(err);
        should.not.exist(contents);
        
      });
    })
    
    done();
  });
  
  it('Valid use', function(done) {
    
    var r = Runner.createRunner({ params: 'local.params.json' });
    
    should.not.Throw(r.run);
    
    r.run(function(err, contents) {
      
      should.not.exist(err);
      should.exist(contents);
      expect(contents).to.have.property('length', 2);
      
      done();
      
    })
    
  })
  
  it.skip('Runner (CLI)', function(done) {
    
    should.not.Throw(function() {
    
      var f = child_process.fork('index', [ '--run']);
      f.on('close', function(code) {
        
        done();
      });

    })
    
  })
  
  it('Invalid use alternatives', function(done) {
    
    var r1 = Runner.createRunner({ params: 'local.params.json' });
    r1.options.mysqldbcompare = 'mys';
    should.not.Throw(r1.run);
    setTimeout(done, 5000);
    
  })
  
  it('Invalid use alternatives 2', function(done) {
    
    var r2 = Runner.createRunner({ params: 'local.params.json' });
    r2.options.mysqldbcompare = 'mys';    
    r2.run(function(err, contents) {
      should.exist(err);
      should.not.exist(contents);
      
      done();
    });
    
  });
  
  it('Invalid use alternatives 3', function(done) {
    
    var r3 = Runner.createRunner({ params: 'local.params.json' });
    delete r3.options.file;
    
    r3.run(function(err, contents) {
      should.not.exist(err);
      should.exist(contents);
      
      done();
      
    });
    
  })
  
})