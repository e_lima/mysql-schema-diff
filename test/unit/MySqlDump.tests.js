var fs = require('fs');

describe('MySqlDump tests', function() {
  
  it('Invalid params', function() {
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDump();
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDump({});
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDump({ 'source-user': '123' });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDump({ 'source-user': '123', 'source-host': '123' });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDump({ 'source-user': '123', 'source-host': '123', 'source-database': '123' });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDump({ 'source-user': '123', 'source-host': '123', 'source-database': '123', 'source-password': '123' });
    })
    
  });
  
  it('Invalid call', function(done) {
    
    should.not.Throw(function() {
      MySqlSchemaDiff.MySqlDump(
        { 'source-user': '123', 'source-host': '123', 'source-database': '123', 'source-password': '123' }, 
        function(err, content) {
          
          expect(content).to.be.equal('');
          should.exist(err);
          done();
          
        }
      );
    })
    
  }) 
  
  it('Valid call', function(done) {
    var okParams = JSON.parse(fs.readFileSync('local.params.json', { encoding: 'utf-8' }));
    okParams.verbose = true;
    delete okParams.file;
    
    MySqlSchemaDiff.MySqlDump(okParams, function(err, content) {
      
      expect(content).to.not.be.equal('');
      should.not.exist(err);
      done();
      
    });
  })
  
})