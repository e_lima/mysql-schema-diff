var fs = require('fs');

describe('MySqlDiff tests', function() { 
  
  it('Invalid params', function(){
    
      should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff();
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff({});
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff({ 'source-user': '123' });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff({ 'source-user': '123', 'source-host': '123' });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff({ 'source-user': '123', 'source-host': '123', 'source-database': '123' });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff({ 'source-user': '123', 'source-host': '123', 'source-database': '123', 'source-password': '123' });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff(
        { 
          'source-user': '123', 
          'source-host': '123', 
          'source-database': '123', 
          'source-password': '123',
          'target-user': '123'
        });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff(
        { 
          'source-user': '123', 
          'source-host': '123', 
          'source-database': '123', 
          'source-password': '123',
          'target-user': '123',
          'target-host': '123'
        });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff(
        { 
          'source-user': '123', 
          'source-host': '123', 
          'source-database': '123', 
          'source-password': '123',
          'target-user': '123',
          'target-host': '123',
          'target-database': '123'
        });
    })
    
    should.Throw(function() {
      MySqlSchemaDiff.MySqlDiff(
        { 
          'source-user': '123', 
          'source-host': '123', 
          'source-database': '123', 
          'source-password': '123',
          'target-user': '123',
          'target-host': '123',
          'target-database': '123',
          'target-password': '123'
        });
    })
    
  })
  
  it('Invalid call 1', function(done) {
    
    should.not.Throw(function() {
      MySqlSchemaDiff.MySqlDiff(
        { 
          'source-user': '123', 
          'source-host': '123', 
          'source-database': '123', 
          'source-password': '123',
          'target-user': '123',
          'target-host': '123',
          'target-database': '123',
          'target-password': '123'
        },
        function(err, diffContent) {
          
          should.exist(diffContent);
          should.exist(err);
          
          done();
          
        }
      );
    })
  })
  
  it('Invalid call 2', function(done) {
    
    var okParams = JSON.parse(fs.readFileSync('local.params.json', { encoding: 'utf-8' }));
    okParams.verbose = true;
    delete okParams.file;
    okParams.mysqldbcompare += 'x';
    
    MySqlSchemaDiff.MySqlDiff(okParams, function(err, content) {
      
      expect(content).to.be.equal('');
      should.exist(err);
      done();
      
    });
    
  })
  
  it('Valid call', function(done) {
    
    var okParams = JSON.parse(fs.readFileSync('local.params.json', { encoding: 'utf-8' }));
    okParams.verbose = true;
    delete okParams.file;
    
    console.log(okParams)
    
    MySqlSchemaDiff.MySqlDiff(okParams, function(err, content) {
      
      expect(content).to.not.be.equal('');
      should.not.exist(err);
      done();
      
    });
  })
  
})