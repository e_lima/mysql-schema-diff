var util = require('util');

describe('local-utils test', function() {
  
  it('local-utils requiredValidation', function() {
    
    should.Throw(function() {
      util.requiredValidation({}, 'p');
    })
    
    should.not.Throw(function() {
      util.requiredValidation({ p: 1}, 'p');
    })
    
  })
  
  it('local-utils ifNotExistsTransform', function() {
    
    expect(util.ifNotExistsTransform('A')).to.be.equal('A');
    expect(util.ifNotExistsTransform('CREATE SOMETHING NAMED')).to.be.equal('CREATE SOMETHING IF NOT EXISTS NAMED');
    
    should.Throw(function() {
      util.ifNotExistsTransform(null);
    });
    
  })
  
})