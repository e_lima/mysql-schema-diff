#re-create the source database
drop database if exists `source-db`;
create database `source-db`;

#re-create the target database
drop database if exists `target-db`;
create database `target-db`;

#create the source table
create table `source-db`.`source_table` (
  id int auto_increment not null primary key
);