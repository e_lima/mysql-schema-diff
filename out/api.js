YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "LocalUtils",
        "MySqlSchemaDiff",
        "Runner"
    ],
    "modules": [
        "core",
        "runner",
        "samples",
        "utility"
    ],
    "allModules": [
        {
            "displayName": "core",
            "name": "core",
            "description": "Provides the utility class for dealing with mysql utilities and commands"
        },
        {
            "displayName": "runner",
            "name": "runner",
            "description": "Runner module for command line execution (CLI)"
        },
        {
            "displayName": "samples",
            "name": "samples",
            "description": "Sample configs"
        },
        {
            "displayName": "utility",
            "name": "utility",
            "description": "Provides the utility classes and functions for internal use"
        }
    ]
} };
});