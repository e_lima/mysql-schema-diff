/**
 * Sample configs
 * @module samples
*/
{ 
  /**
   * User to use when connecting to source host database
   * @property source-user
   * @type {String}
  */
  "source-user": "root",
  /**
   * Source database host name
   * @property source-host
   * @type {String}
  */
  "source-host": "localhost",
  /**
   * Password to use when connecting to source host database
   * @property source-password
   * @type {String}
  */
  "source-password": "makro123",
  /**
   * Schema/Database on source host
   * @property source-database
   * @type {String}
  */
  "source-database": "bandchest",
  /**
   * User to use when connecting to target host database
   * @property target-user
   * @type {String}
  */
  "target-user": "root",
  /**
   * Target database host name
   * @property target-host
   * @type {String}
  */
  "target-host": "localhost",
  /**
   * Password to use when connecting to target host database
   * @property target-password
   * @type {String}
  */
  "target-password": "makro123",
  /**
   * Schema/Database on target host
   * @property target-database
   * @type {String}
  */
  "target-database": "ebdb",
  /**
   * Script output file name
   * Will contain the SQL script necessary to transform the target database
   * @property file
   * @type {String}
  */
  "file": "dump.sql",
  /**
   * Flag for change the default CREATE TABLE statement to CREATE TABLE IF NOT EXISTS
   * @property create-if-not-exists
   * @type {String}
   * @default false
  */
  "create-if-not-exists": true,
  /**
   * mysqldump command path
   * @property mysqldump
   * @type {String}
  */
  "mysqldump": "c:\\Program Files\\MySQL\\MySQL Server 5.6\\bin\\mysqldump",
  /**
   * mysqldbcompare command path
   * @property mysqldbcompare
   * @type {String}
  */
  "mysqldbcompare": "c:\\Program Files\\MySQL\\MySQL Utilities\\mysqldbcompare",
  /**
   * Do(or not) write to stdio
   * @property silent
   * @type {String}
   * @default false
  */
  "silent": true,
  /**
   * Do(or not) write SQL to stdio
   * @property verbose
   * @type {String}
   * @default false
  */
  "verbose": false
}